package com.prisyazhnuy.universeradio.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;

public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_about, container, false);
        TextView tvVersion = (TextView) inflate.findViewById(R.id.tvVersion);
        tvVersion.setText(String.format(getString(R.string.version_text), getString(R.string.version)));
        TextView tvLink = (TextView) inflate.findViewById(R.id.tvLink);
        if (tvLink != null) {
            tvLink.setMovementMethod(LinkMovementMethod.getInstance());
        }
        return inflate;
    }

}
