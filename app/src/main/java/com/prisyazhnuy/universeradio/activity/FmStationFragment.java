package com.prisyazhnuy.universeradio.activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.controller.FmStationAdapter;
import com.prisyazhnuy.universeradio.model.FmStation;

import java.util.ArrayList;
import java.util.List;

public class FmStationFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;

    public FmStationFragment() {
    }

    public static FmStationFragment newInstance() {
        return new FmStationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fmstation_list, container, false);
        RecyclerView rvStations = (RecyclerView) view.findViewById(R.id.list);
        rvStations.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvStations.setAdapter(new FmStationAdapter(buildFmStations(), mListener));
        return view;
    }

    private List<FmStation> buildFmStations() {
        List<FmStation> stations = new ArrayList<>();
        String[] stationsStr = getResources().getStringArray(R.array.stations);
        for (String station : stationsStr) {
            stations.add(new FmStation(station));
        }
        return stations;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FmStation item);
    }
}
