package com.prisyazhnuy.universeradio.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.controller.MediaPlayerService;
import com.prisyazhnuy.universeradio.controller.PlayerStateReceiver;
import com.prisyazhnuy.universeradio.controller.RadioManager;
import com.prisyazhnuy.universeradio.controller.RadioStationAdapter;
import com.prisyazhnuy.universeradio.dialog.CustomDialog;
import com.prisyazhnuy.universeradio.dialog.EmptyDialog;
import com.prisyazhnuy.universeradio.dialog.MessageDialog;
import com.prisyazhnuy.universeradio.dialog.PositiveButton;
import com.prisyazhnuy.universeradio.dialog.TitleDialog;
import com.prisyazhnuy.universeradio.model.FmStation;
import com.prisyazhnuy.universeradio.model.PlayerStateListener;
import com.prisyazhnuy.universeradio.model.RadioStation;

public class MainActivity extends AppCompatActivity implements PlayerStateListener, NavigationView.OnNavigationItemSelectedListener, FmStationFragment.OnListFragmentInteractionListener {

    private static final String ACTION_PLAY = "action.PLAY";
    private static final String ACTION_PAUSE = "action.PAUSE";
    private static final String ACTION_NEXT = "action.NEXT";
    private static final String ACTION_PREV = "action.PREV";

    private PlayerStateReceiver mPlayerReceiver;

    private AudioManager audioManager;
    private SeekBar skbVolume;
    private ListView mStationListView;
    private TextView tvNameStation;
    private ImageButton iBtnPlayPause;
    private FmStationFragment mStationsFragment;
    private Fragment mAboutFragment;


    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationView.getMenu().getItem(0).setChecked(true);

        skbVolume = (SeekBar) findViewById(R.id.skbVolume);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        skbVolume.setMax(maxVolume);
        skbVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ImageButton iBtnPrevStation = (ImageButton) findViewById(R.id.ibtnPrevStation);
        iBtnPrevStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent prevIntent = new Intent(MainActivity.this, MediaPlayerService.class);
                prevIntent.setAction(ACTION_PREV);
                startService(prevIntent);
            }
        });
        ImageButton iBtnNextStation = (ImageButton) findViewById(R.id.ibtnNextStation);
        iBtnNextStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextIntent = new Intent(MainActivity.this, MediaPlayerService.class);
                nextIntent.setAction(ACTION_NEXT);
                startService(nextIntent);
            }
        });
        RadioStation currentRadioStation = RadioManager.getInstance().getCurrentRadioStation();
        mStationListView = (ListView) findViewById(R.id.lvStations);
        tvNameStation = (TextView) findViewById(R.id.tvStationName);
        tvNameStation.setText(currentRadioStation.getName());
        iBtnPlayPause = (ImageButton) findViewById(R.id.ibtnPlayPause);
        if (currentRadioStation.isPlaying()) {
            iBtnPlayPause.getDrawable().setLevel(1);
        } else {
            iBtnPlayPause.getDrawable().setLevel(0);
        }
        iBtnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioStation currentRadioStation = RadioManager.getInstance().getCurrentRadioStation();
                Intent intent = new Intent(MainActivity.this, MediaPlayerService.class);
                if (currentRadioStation.isPlaying()) {
                    intent.setAction(ACTION_PAUSE);
                } else {
                    intent.setAction(ACTION_PLAY);
                }
                startService(intent);
            }
        });

        String versionText = String.format(getString(R.string.version_text), getString(R.string.version));
        TextView tvVersion = (TextView) findViewById(R.id.tvVersion);
        tvVersion.setText(versionText);

        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction("PlayerState");
        mFilter.addCategory(Intent.CATEGORY_DEFAULT);
        mPlayerReceiver = new PlayerStateReceiver(this);
        registerReceiver(mPlayerReceiver, mFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        skbVolume.setProgress(volume);
        initRadioStationsList();
    }

    @Override
    protected void onDestroy() {
        if (mPlayerReceiver != null) {
            unregisterReceiver(mPlayerReceiver);
        }
        super.onDestroy();
    }

    private void initRadioStationsList() {
        final ArrayAdapter<RadioStation> radioStationAdapter = new RadioStationAdapter(this, RadioManager.getInstance().getRadioStations());
        mStationListView.setAdapter(radioStationAdapter);
        mStationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent playService = new Intent(MainActivity.this, MediaPlayerService.class);
                RadioStation radioStation = radioStationAdapter.getItem(position);
                if (radioStation != null) {
                    if (radioStation.isPlaying()) {
                        playService.setAction(ACTION_PAUSE);
                    } else {
                        playService.setAction(ACTION_PLAY);
                    }
                    playService.putExtra("name", radioStation.getName());
                    startService(playService);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
                Log.d("MainActivity", "volume = " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                skbVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                break;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
                Log.d("MainActivity", "volume = " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                skbVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                break;
            case KeyEvent.KEYCODE_BACK:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void play(RadioStation station) {
        ((ArrayAdapter) mStationListView.getAdapter()).notifyDataSetChanged();
        int position = ((ArrayAdapter) mStationListView.getAdapter()).getPosition(station);
        mStationListView.setSelection(position);
        tvNameStation.setText(station.getName() + " " + station.getName() + " " + station.getName() + " " + station.getName() + " " + station.getName() + " " + station.getName() + " " + station.getName() + " " + station.getName() + " ");
        iBtnPlayPause.getDrawable().setLevel(1);
    }

    @Override
    public void pause(RadioStation station) {
        ((ArrayAdapter) mStationListView.getAdapter()).notifyDataSetChanged();
        int position = ((ArrayAdapter) mStationListView.getAdapter()).getPosition(station);
        mStationListView.setSelection(position);
        iBtnPlayPause.getDrawable().setLevel(0);
    }

    @Override
    public void error(RadioStation station) {
        View.OnClickListener positiveClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
        String dialogPositiveBtn = getString(R.string.ok);
        String dialogTitle = getString(R.string.app_name);
        String dialogMessage = String.format(getString(R.string.error_message), station.getName());
        CustomDialog firstPermissionDialog = new TitleDialog(
                new MessageDialog(
                        new PositiveButton(
                                new EmptyDialog(this), dialogPositiveBtn, positiveClick),
                        dialogMessage),
                dialogTitle);
        firstPermissionDialog.show();
    }

    public void playPauseAction(View view) {
        Intent playService = new Intent(MainActivity.this, MediaPlayerService.class);
        int position = (Integer) view.getTag();
        RadioStation radioStation = (RadioStation) mStationListView.getAdapter().getItem(position);
        if (radioStation != null) {
            if (radioStation.isPlaying()) {
                playService.setAction(ACTION_PAUSE);
            } else {
                playService.setAction(ACTION_PLAY);
            }
            playService.putExtra("name", radioStation.getName());
            startService(playService);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mNavigationView.getMenu().getItem(0).isChecked()) {
                super.onBackPressed();
            } else {
                activateRadioPage();
            }
        }
    }

    private void activateRadioPage() {
        mNavigationView.getMenu().getItem(0).setChecked(true);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.app_name);
        }
        if (mStationsFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(mStationsFragment).commitAllowingStateLoss();
        }
        if (mAboutFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(mAboutFragment).commitAllowingStateLoss();
        }
    }

    private void activateFmStationsPage() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.fm_station_title);
        }
        if (mAboutFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(mAboutFragment).commitAllowingStateLoss();
        }
        if (mStationsFragment == null) {
            mStationsFragment = FmStationFragment.newInstance();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.content_main, mStationsFragment).commitAllowingStateLoss();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (!item.isChecked()) {
            switch (item.getItemId()) {
                case R.id.nav_radio:
                    activateRadioPage();
                    break;
                case R.id.nav_fm_stations:
                    activateFmStationsPage();
                    break;
                case R.id.nav_about:
                    activateAboutPage();
                    break;
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void activateAboutPage() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.about);
        }
        if (mStationsFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(mStationsFragment).commitAllowingStateLoss();
        }
        if (mAboutFragment == null) {
            mAboutFragment = new AboutFragment();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.content_main, mAboutFragment).commitAllowingStateLoss();
    }

    @Override
    public void onListFragmentInteraction(FmStation item) {

    }

    public void socialClick(View view) {
        int linkTextId = 0;
        switch (view.getId()) {
            case R.id.ivFacebook:
                linkTextId = R.string.facebook_link;
                break;
            case R.id.ivInstagram:
                linkTextId = R.string.instagram_link;
                break;
            case R.id.ivTwitter:
                linkTextId = R.string.twitter_link;
                break;
            case R.id.ivVk:
                linkTextId = R.string.vk_link;
                break;
        }
        if (linkTextId != 0) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(linkTextId)));
            browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (browserIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(browserIntent);
            }
        }
    }
}
