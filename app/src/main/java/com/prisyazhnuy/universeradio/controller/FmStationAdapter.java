package com.prisyazhnuy.universeradio.controller;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.activity.FmStationFragment;
import com.prisyazhnuy.universeradio.model.FmStation;

import java.util.List;

public class FmStationAdapter extends RecyclerView.Adapter<FmStationAdapter.ViewHolder> {

    private final List<FmStation> mValues;
    private final FmStationFragment.OnListFragmentInteractionListener mListener;

    public FmStationAdapter(List<FmStation> items, FmStationFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_fmstation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mFmStation = mValues.get(position);
        holder.mTvCity.setText(mValues.get(position).getCity());
        holder.mTvFrequency.setText(String.valueOf(mValues.get(position).getFrequency()));
        holder.mTvPopulation.setText(String.valueOf(mValues.get(position).getPopulation()));
        if (position % 2 == 0) {
            holder.mLlStation.setBackgroundResource(R.color.grey_400_50);
        } else {
            holder.mLlStation.setBackgroundResource(R.color.grey_600_50);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mFmStation);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mTvCity;
        final TextView mTvFrequency;
        final TextView mTvPopulation;
        final LinearLayout mLlStation;
        FmStation mFmStation;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mTvCity = (TextView) view.findViewById(R.id.tvCity);
            mTvFrequency = (TextView) view.findViewById(R.id.tvFrequency);
            mTvPopulation = (TextView) view.findViewById(R.id.tvPopulation);
            mLlStation = (LinearLayout) view.findViewById(R.id.llStation);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvCity.getText() + "'";
        }
    }
}
