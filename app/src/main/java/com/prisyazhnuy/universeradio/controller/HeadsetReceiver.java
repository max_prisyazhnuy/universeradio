package com.prisyazhnuy.universeradio.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import com.prisyazhnuy.universeradio.model.HeadsetListener;

public class HeadsetReceiver extends BroadcastReceiver {

    private HeadsetListener listener;

    public HeadsetReceiver(HeadsetListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
            if (listener != null) {
                listener.unpluged();
            }
        }
    }
}
