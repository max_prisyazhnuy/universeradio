package com.prisyazhnuy.universeradio.controller;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.prisyazhnuy.universeradio.activity.MainActivity;
import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.model.HeadsetListener;
import com.prisyazhnuy.universeradio.model.RadioStation;

import java.io.IOException;

public class MediaPlayerService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, HeadsetListener {

    private static final String ACTION_PLAY = "action.PLAY";
    private static final String ACTION_PAUSE = "action.PAUSE";
    private static final String ACTION_CLOSE = "action.CLOSE";
    private static final String ACTION_NEXT = "action.NEXT";
    private static final String ACTION_PREV = "action.PREV";
    private static final String ACTION_ERROR = "action.ERROR";
    private HeadsetReceiver mHeadsetReceiver;
    private int mVolume;

    private MediaPlayer mMediaPlayer = null;
    private RadioManager mRadioManager;
    private boolean isError = false;

    private AudioManager mAudioManager;
    private final static int MIN_VOLUME = 1;
    private AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            Log.e("MediaPlayerService", "onAudioFocusChange focusChange = " + focusChange);
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS:
                    Log.e("MediaPlayerService", "onAudioFocusChange focusChange = AUDIOFOCUS_LOSS");
                    // Permanent loss of audio focus
                    // Pause playback immediately
                    // Wait 30 seconds before stopping playback
                    stopPlayer();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    // Pause playback
                    Log.e("MediaPlayerService", "onAudioFocusChange focusChange = AUDIOFOCUS_LOSS_TRANSIENT");
                    stopPlayer();
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    // Lower the volume, keep playing
                    Log.e("MediaPlayerService", "onAudioFocusChange focusChange = AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                    mVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, MIN_VOLUME, 0);
                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                    // Your app has been granted audio focus again
                    // Raise volume to normal, restart playback if necessary
                    Log.e("MediaPlayerService", "onAudioFocusChange focusChange = AUDIOFOCUS_GAIN");
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mVolume, 0);
                    break;
            }
        }
    };

    public MediaPlayerService() {
        mMediaPlayer = new MediaPlayer();
        mRadioManager = RadioManager.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case ACTION_CLOSE:
                sendPauseState();
                stopSelf();
                break;
            case ACTION_PLAY: {
                sendPauseState();
                String name = intent.getStringExtra("name");
                RadioStation radioStation = mRadioManager.findByName(name);
                startPlayer(radioStation);
                break;
            }
            case ACTION_PAUSE: {
                stopPlayer();
                break;
            }
            case ACTION_NEXT: {
                sendPauseState();
                RadioStation nextStation = mRadioManager.getNextRadioStation();
                startPlayer(nextStation);
                break;
            }
            case ACTION_PREV: {
                sendPauseState();
                RadioStation prevStation = mRadioManager.getPrevRadioStation();
                startPlayer(prevStation);
                break;
            }
        }
        return START_NOT_STICKY;
    }

    private void sendPauseState() {
        mRadioManager.getCurrentRadioStation().setPlaying(false);
        Intent intent = new Intent("PlayerState");
        intent.putExtra("state", ACTION_PAUSE);
        intent.putExtra("station", mRadioManager.getCurrentRadioStation());
        sendBroadcast(intent);
    }

    private void sendErrorState() {
        Intent intent = new Intent("PlayerState");
        intent.putExtra("state", ACTION_ERROR);
        intent.putExtra("station", mRadioManager.getCurrentRadioStation());
        sendBroadcast(intent);
    }

    private void showPlayNotification(RadioStation radioStation) {
        RemoteViews remoteViews = buildRemoteViews(true, radioStation);
        buildNotification(R.drawable.ic_play_arrow_white_24dp, remoteViews);
    }

    private void showPauseNotification(RadioStation radioStation) {
        RemoteViews remoteViews = buildRemoteViews(false, radioStation);
        buildNotification(R.drawable.ic_pause_white_24dp, remoteViews);
    }

    public void updateWidget(RadioStation station) {
        Intent intent = new Intent(this, RadioWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra("station", station);
        int[] ids = AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, RadioWidgetProvider.class));
        if (ids != null && ids.length > 0) {
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
            sendBroadcast(intent);
        }
    }

    private RemoteViews buildRemoteViews(boolean isPlay, RadioStation radioStation) {
        Intent notifyIntent = new Intent(this, MediaPlayerService.class);
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_layout);
        if (isPlay) {
            notifyIntent.setAction(ACTION_PAUSE);
            remoteViews.setImageViewResource(R.id.ibtnPlay, R.drawable.ic_pause_black_24dp);
        } else {
            notifyIntent.setAction(ACTION_PLAY);
            remoteViews.setImageViewResource(R.id.ibtnPlay, R.drawable.ic_play_arrow_black_24dp);
        }
        remoteViews.setTextViewText(R.id.tvTitle, radioStation.getName());
        PendingIntent notifyPendingIntent = PendingIntent.getService(this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnPlay, notifyPendingIntent);
        Intent closeIntent = new Intent(this, MediaPlayerService.class);
        closeIntent.setAction(ACTION_CLOSE);
        PendingIntent closePendingIntent = PendingIntent.getService(this, 0, closeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnClose, closePendingIntent);

        Intent nextIntent = new Intent(this, MediaPlayerService.class);
        nextIntent.setAction(ACTION_NEXT);
        PendingIntent nextPendingIntent = PendingIntent.getService(this, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnNext, nextPendingIntent);

        Intent prevIntent = new Intent(this, MediaPlayerService.class);
        prevIntent.setAction(ACTION_PREV);
        PendingIntent prevPendingIntent = PendingIntent.getService(this, 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnPrev, prevPendingIntent);

        return remoteViews;
    }

    private void buildNotification(int smallIcon, RemoteViews remoteViews) {
        if (remoteViews != null) {
            NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
            notification.setSmallIcon(smallIcon);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                notification.setCustomContentView(remoteViews);
            } else {
                //noinspection deprecation
                notification.setContent(remoteViews);
            }
            Intent mainActivity = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mainActivity, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(pendingIntent);
            notification.setPriority(Integer.MAX_VALUE);
            startForeground(100, notification.build());
        }
    }

    private void startPlayer(RadioStation radioStation) {
        if (radioStation != null) {
            try {
                mMediaPlayer.reset();
                int result = mAudioManager.requestAudioFocus(afChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    IntentFilter mFilter = new IntentFilter();
                    mFilter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
                    mHeadsetReceiver = new HeadsetReceiver(this);
                    registerReceiver(mHeadsetReceiver, mFilter);
                    // Start playback
                    radioStation.setPlaying(true);
                    mMediaPlayer.setDataSource(radioStation.getUrl());
//                    mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                    mMediaPlayer.setOnPreparedListener(this);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setOnErrorListener(this);
                    mMediaPlayer.prepareAsync(); // prepare async to not block main thread
                    showPlayNotification(radioStation);
                    updateWidget(radioStation);
                    Intent intent = new Intent("PlayerState");
                    intent.putExtra("state", ACTION_PLAY);
                    intent.putExtra("station", radioStation);
                    sendBroadcast(intent);
                }
            } catch (IOException e) {
                Log.e("MediaPlayerService", "exception", e);
            }
        }
    }

    private void stopPlayer() {
        if (mMediaPlayer.isPlaying()) {
            if (mHeadsetReceiver != null) {
                unregisterReceiver(mHeadsetReceiver);
                mHeadsetReceiver = null;
            }
            mAudioManager.abandonAudioFocus(afChangeListener);
            sendPauseState();
            updateWidget(mRadioManager.getCurrentRadioStation());
            mMediaPlayer.pause();
            mMediaPlayer.reset();
            showPauseNotification(mRadioManager.getCurrentRadioStation());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        stopPlayer();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        stopForeground(true);
        super.onDestroy();
    }

    public void onPrepared(MediaPlayer player) {
        isError = false;
        player.start();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e("MediaPlayerService", "onError what = " + what + " extra = " + extra);
        isError = true;
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.e("MediaPlayerService", "onCompletion");
        if (!isError) {
            startPlayer(mRadioManager.getCurrentRadioStation());
        } else {
            stopPlayer();
            sendErrorState();
        }
    }

    @Override
    public void unpluged() {
        if (mRadioManager.getCurrentRadioStation().isPlaying()) {
            stopPlayer();
        }
    }
}
