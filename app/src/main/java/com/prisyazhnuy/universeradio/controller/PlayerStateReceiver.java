package com.prisyazhnuy.universeradio.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.prisyazhnuy.universeradio.model.PlayerStateListener;
import com.prisyazhnuy.universeradio.model.RadioStation;

public class PlayerStateReceiver extends BroadcastReceiver {

    private static final String ACTION_PLAY = "action.PLAY";
    private static final String ACTION_PAUSE = "action.PAUSE";
    private static final String ACTION_ERROR = "action.ERROR";

    private PlayerStateListener listener;

    public PlayerStateReceiver(PlayerStateListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        RadioStation station = (RadioStation) intent.getSerializableExtra("station");
        switch (intent.getStringExtra("state")) {
            case ACTION_PLAY:
                listener.play(station);
                break;
            case ACTION_PAUSE:
                listener.pause(station);
                break;
            case ACTION_ERROR:
                listener.error(station);
                break;
        }
    }
}
