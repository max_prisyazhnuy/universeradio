package com.prisyazhnuy.universeradio.controller;

import com.prisyazhnuy.universeradio.model.RadioStation;

import java.util.List;

public class RadioManager {

    private List<RadioStation> radioStations = RadioStationFactory.buildAllStations();
    private int mCurrentStationPosition = 0;
    private static RadioManager instance;

    public static RadioManager getInstance() {
        if (instance == null) {
            instance = new RadioManager();
        }
        return instance;
    }

    public List<RadioStation> getRadioStations() {
        return radioStations;
    }

    private RadioManager() {
    }

    public RadioStation findByName(String name) {
        RadioStation searchStation = new RadioStation(name, "");
        if (radioStations.contains(searchStation)) {
            for (int i = 0; i < radioStations.size(); i++) {
                if (radioStations.get(i).equals(searchStation)) {
                    mCurrentStationPosition = i;
                    break;
                }
            }
        }
        return radioStations.get(mCurrentStationPosition);
    }

    public int findPosition(RadioStation station) {
        int position = -1;
        if (radioStations.contains(station)) {
            for (int i = 0; i < radioStations.size(); i++) {
                if (radioStations.get(i).equals(station)) {
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    public RadioStation getNextRadioStation() {
        if (mCurrentStationPosition == radioStations.size() - 1) {
            mCurrentStationPosition = 0;
        } else {
            mCurrentStationPosition ++;
        }
        return radioStations.get(mCurrentStationPosition);
    }

    public RadioStation getPrevRadioStation() {
        if (mCurrentStationPosition == 0) {
            mCurrentStationPosition = radioStations.size() - 1;
        } else {
            mCurrentStationPosition --;
        }
        return radioStations.get(mCurrentStationPosition);
    }

    public RadioStation getCurrentRadioStation() {
        return radioStations.get(mCurrentStationPosition);
    }

}
