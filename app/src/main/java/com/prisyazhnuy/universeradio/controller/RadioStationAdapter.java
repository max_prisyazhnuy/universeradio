package com.prisyazhnuy.universeradio.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.model.RadioStation;

import java.util.List;

public class RadioStationAdapter extends ArrayAdapter<RadioStation> {

    private final Context mContext;
    private final List<RadioStation> mStations;

    public RadioStationAdapter(Context context, List<RadioStation> objects) {
        super(context, R.layout.radio_station_layout, objects);
        this.mContext = context;
        this.mStations = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.radio_station_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvStationName);
            viewHolder.btnPlay = (ImageButton) convertView.findViewById(R.id.ibtnPlayPause);
            viewHolder.rlBackground = (RelativeLayout) convertView.findViewById(R.id.rlOnline);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        RadioStation radioStation = mStations.get(position);
        if(radioStation != null) {
            viewHolder.tvTitle.setText(radioStation.getName());
            viewHolder.btnPlay.setTag(position);
            if (radioStation.isPlaying()) {
                viewHolder.btnPlay.getDrawable().setLevel(1);
                viewHolder.rlBackground.setBackgroundResource(R.color.station_item_background);
            } else {
                viewHolder.btnPlay.getDrawable().setLevel(0);
                viewHolder.rlBackground.setBackgroundResource(android.R.color.transparent);
            }
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView tvTitle;
        ImageButton btnPlay;
        RelativeLayout rlBackground;
    }
}
