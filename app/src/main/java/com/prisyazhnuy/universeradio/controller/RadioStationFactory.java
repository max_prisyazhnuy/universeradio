package com.prisyazhnuy.universeradio.controller;

import com.prisyazhnuy.universeradio.model.RadioStation;

import java.util.ArrayList;
import java.util.List;

public class RadioStationFactory {

    private static final String urlOnline = "http://cast.nrj.in.ua/nrj";
    private static final String urlPartyHits = "http://cast2.nrj.in.ua/nrj_party";
    private static final String urlAllHits = "http://cast2.nrj.in.ua/nrj_hits";
    private static final String urlTop40 = "http://cast2.nrj.in.ua/nrj_hot";
    private static final String urlKissFm = "http://online-kissfm.tavrmedia.ua/KissFM";
    private static final String urlKissFmDeep = "http://online-kissfm.tavrmedia.ua/KissFM_deep";
    private static final String urlKissFmDigital = "http://online-kissfm.tavrmedia.ua/KissFM_digital";
    private static final String urlKissFm32 = "http://online-kissfm.tavrmedia.ua/KissFM_32";

    public static List<RadioStation> buildAllStations() {
        List<RadioStation> radioStations = new ArrayList<>();
        radioStations.add(new RadioStation("Online", urlOnline));
        radioStations.add(new RadioStation("Top 40", urlTop40));
        radioStations.add(new RadioStation("All hits", urlAllHits));
        radioStations.add(new RadioStation("Party hits", urlPartyHits));
        radioStations.add(new RadioStation("Kiss fm", urlKissFm));
        radioStations.add(new RadioStation("Kiss fm deep", urlKissFmDeep));
        radioStations.add(new RadioStation("Kiss fm digital", urlKissFmDigital));
        return radioStations;
    }
}
