package com.prisyazhnuy.universeradio.controller;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.prisyazhnuy.universeradio.R;
import com.prisyazhnuy.universeradio.activity.MainActivity;
import com.prisyazhnuy.universeradio.model.RadioStation;

public class RadioWidgetProvider extends AppWidgetProvider {

    private static final String ACTION_PLAY = "action.PLAY";
    private static final String ACTION_PAUSE = "action.PAUSE";
    private static final String ACTION_NEXT = "action.NEXT";
    private static final String ACTION_PREV = "action.PREV";

    private RadioStation station;

    @Override
    public void onReceive(Context context, Intent intent) {
        station = (RadioStation) intent.getSerializableExtra("station");
        super.onReceive(context, intent);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            if (station == null) {
                RadioManager radioManager = RadioManager.getInstance();
                station = radioManager.getCurrentRadioStation();
            }
            appWidgetManager.updateAppWidget(appWidgetId, buildRemoteViews(context, station.isPlaying(), station));
        }
    }

    private RemoteViews buildRemoteViews(Context context, boolean isPlay, RadioStation radioStation) {
        Intent notifyIntent = new Intent(context, MediaPlayerService.class);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        if (isPlay) {
            notifyIntent.setAction(ACTION_PAUSE);
            remoteViews.setImageViewResource(R.id.ibtnPlay, R.drawable.ic_pause_black_24dp);
        } else {
            notifyIntent.setAction(ACTION_PLAY);
            remoteViews.setImageViewResource(R.id.ibtnPlay, R.drawable.ic_play_arrow_black_24dp);
        }
        remoteViews.setTextViewText(R.id.tvTitle, radioStation.getName());
        PendingIntent notifyPendingIntent = PendingIntent.getService(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnPlay, notifyPendingIntent);

        Intent nextIntent = new Intent(context, MediaPlayerService.class);
        nextIntent.setAction(ACTION_NEXT);
        PendingIntent nextPendingIntent = PendingIntent.getService(context, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnNext, nextPendingIntent);

        Intent prevIntent = new Intent(context, MediaPlayerService.class);
        prevIntent.setAction(ACTION_PREV);
        PendingIntent prevPendingIntent = PendingIntent.getService(context, 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ibtnPrev, prevPendingIntent);

        Intent mainIntent = new Intent(context, MainActivity.class);
        PendingIntent mainPendingIntent = PendingIntent.getActivity(context, 0, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.rlWidget, mainPendingIntent);

        return remoteViews;
    }

}
