package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;

public interface CustomDialog {
    void show();
    Dialog build();
    void hide();
}
