package com.prisyazhnuy.universeradio.dialog;

public abstract class DialogDecorator implements CustomDialog {

    protected CustomDialog decoratedDialog;

    public DialogDecorator(CustomDialog decoratedDialog) {
        this.decoratedDialog = decoratedDialog;
    }

    public void show() {
        decoratedDialog.show();
    }

}
