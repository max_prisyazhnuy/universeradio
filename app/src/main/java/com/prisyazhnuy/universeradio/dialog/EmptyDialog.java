package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.prisyazhnuy.universeradio.R;

public class EmptyDialog implements CustomDialog {

    private final Dialog dialog;

    public EmptyDialog(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
    }

    @Override
    public void show() {
        dialog.show();
    }

    @Override
    public Dialog build() {
        return dialog;
    }

    @Override
    public void hide() {
        dialog.dismiss();
    }

}
