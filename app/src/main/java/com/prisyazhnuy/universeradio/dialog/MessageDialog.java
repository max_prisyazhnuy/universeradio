package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;

public class MessageDialog extends DialogDecorator {
    protected final String mMessage;

    public MessageDialog(CustomDialog decoratedDialog, String message) {
        super(decoratedDialog);
        this.mMessage = message;
    }

    @Override
    public void show() {
        TextView tvMessage = (TextView) decoratedDialog.build().findViewById(R.id.tvDialogMessage);
        tvMessage.setText(mMessage);
        decoratedDialog.show();
    }

    @Override
    public Dialog build() {
        return decoratedDialog.build();
    }

    @Override
    public void hide() {
        decoratedDialog.hide();
    }
}
