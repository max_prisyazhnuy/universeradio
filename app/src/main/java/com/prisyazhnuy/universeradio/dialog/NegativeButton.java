package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;

import com.prisyazhnuy.universeradio.R;

public class NegativeButton extends DialogDecorator implements View.OnClickListener {

    private final String mText;
    private final View.OnClickListener mClickListener;

    public NegativeButton(CustomDialog decoratedDialog, String buttonTitle, View.OnClickListener buttonListener) {
        super(decoratedDialog);
        this.mText = buttonTitle;
        this.mClickListener = buttonListener;
    }

    @Override
    public void show() {
        Button btnNegative = (Button) decoratedDialog.build().findViewById(R.id.btnNegative);
        btnNegative.setVisibility(View.VISIBLE);
        btnNegative.setText(mText);
        btnNegative.setOnClickListener(this);
        decoratedDialog.show();
    }

    @Override
    public Dialog build() {
        return decoratedDialog.build();
    }

    @Override
    public void hide() {
        decoratedDialog.hide();
    }

    @Override
    public void onClick(View v) {
        mClickListener.onClick(v);
        decoratedDialog.build().dismiss();
    }
}
