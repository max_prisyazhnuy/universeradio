package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;

import com.prisyazhnuy.universeradio.R;

public class PositiveButton extends DialogDecorator implements View.OnClickListener {
    private final String mText;
    private final View.OnClickListener mClickListener;

    public PositiveButton(CustomDialog decoratedDialog, String buttonTitle, View.OnClickListener buttonListener) {
        super(decoratedDialog);
        this.mText = buttonTitle;
        this.mClickListener = buttonListener;
    }

    @Override
    public void show() {
        Button btnPositive = (Button) decoratedDialog.build().findViewById(R.id.btnPositive);
        btnPositive.setText(mText);
        btnPositive.setOnClickListener(this);
        decoratedDialog.show();
    }

    @Override
    public Dialog build() {
        return decoratedDialog.build();
    }

    @Override
    public void hide() {
        decoratedDialog.hide();
    }

    @Override
    public void onClick(View v) {
        mClickListener.onClick(v);
        decoratedDialog.build().dismiss();
    }
}
