package com.prisyazhnuy.universeradio.dialog;

import android.app.Dialog;
import android.widget.TextView;

import com.prisyazhnuy.universeradio.R;

public class TitleDialog extends DialogDecorator {
    protected final String mTitle;

    public TitleDialog(CustomDialog decoratedDialog, String title) {
        super(decoratedDialog);
        this.mTitle = title;
    }

    public void show() {
        TextView tvTitle = (TextView) decoratedDialog.build().findViewById(R.id.tvDialogTitle);
        tvTitle.setText(mTitle);
        decoratedDialog.show();
    }

    @Override
    public Dialog build() {
        return decoratedDialog.build();
    }

    @Override
    public void hide() {
        decoratedDialog.hide();
    }
}
