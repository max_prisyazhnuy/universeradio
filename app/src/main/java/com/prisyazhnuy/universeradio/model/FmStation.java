package com.prisyazhnuy.universeradio.model;

public class FmStation {
    private String city;
    private float frequency;
    private int population;

    public FmStation(String city, float frequency, int population) {
        this.city = city;
        this.frequency = frequency;
        this.population = population;
    }

    public FmStation(String fullInfo) {
        String[] parts = fullInfo.split("[|]");
        this.city = parts[0];
        this.frequency = Float.parseFloat(parts[1]);
        this.population = Integer.parseInt(parts[2]);
    }

    public String getCity() {
        return city;
    }

    public float getFrequency() {
        return frequency;
    }

    public int getPopulation() {
        return population;
    }
}
