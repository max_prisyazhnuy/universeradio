package com.prisyazhnuy.universeradio.model;

public interface PlayerStateListener {
    void play(RadioStation station);
    void pause(RadioStation station);
    void error(RadioStation station);
}
