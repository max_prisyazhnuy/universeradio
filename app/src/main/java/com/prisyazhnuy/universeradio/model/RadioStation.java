package com.prisyazhnuy.universeradio.model;

import java.io.Serializable;

public class RadioStation implements Serializable {

    private String name;
    private String url;
    private boolean isPlaying = false;
    private boolean isFavourite = false;

    public RadioStation(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof RadioStation) {
            return ((RadioStation) obj).getName().equals(name);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "RadioStation: " + name;
    }
}
