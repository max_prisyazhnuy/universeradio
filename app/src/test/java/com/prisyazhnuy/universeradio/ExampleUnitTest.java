package com.prisyazhnuy.universeradio;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void task1() {
        double a = 0;
        double b = 0;
        double c = 0;

        calculate(a, b, c);

    }

    private void calculate(double a, double b, double c) {
        double d = D(a, b, c);
        if (d > 0) {
            if (a == 0) {
                if (c == 0) {
                    System.out.println("x1=0.0");
                    System.out.println("x2=0.0");
                } else {
                    double x1 = (-1 * c) / b;
                    System.out.println("x1=" + x1);
                    System.out.println("x2=" + x1);
                }
            } else {
                double x1 = (-b + Math.sqrt(d)) / (2 * a);
                double x2 = (-b - Math.sqrt(d)) / (2 * a);
                System.out.println("x1=" + x1);
                System.out.println("x2=" + x2);
            }
        } else {
            if (d == 0) {
                if (a != 0) {
                    double x = -b / (2 * a);
                    System.out.println("x1=" + x);
                    System.out.println("x2=" + x);
                } else {
                    System.out.println("x1=");
                    System.out.println("x2=");
                }
            } else {
                System.out.println("x1=");
                System.out.println("x2=");
            }
        }
    }

    private double D(double a, double b, double c) {
        return b*b - 4*a*c;
    }
}